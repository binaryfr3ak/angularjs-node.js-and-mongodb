function logCar(logMsg, callback)
{
	process.nextTick(function(){
		callback(logMsg);
	});
}

var cars = ["Ferrari", "Porsche", "Bugatti", "Aston Martin"];

for(var index in cars)
{
	var message = "Saw a " + cars[index];
	logCar(message, function(){
		console.log("Normal callback" + message);
	});
}

for(var index in cars)
{
	var message = "Saw a " + cars[index];
	(function(msg){
		logCar(msg, function(){
			console.log("Closure callback: " + msg);
		});
	})(message);
}

