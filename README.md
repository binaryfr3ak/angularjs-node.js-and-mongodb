# README #

I created this samples to get an easy walktrought in javascript how it's used in node.js and angularjs.

### What is this repository for? ###

____

This repository contains a couple of sample applications written in javascript, which will be executed with Node.js. I created this repo for other enthusiastic developer which like to read my samples and hope it will give them a little help to developer their own javascript applications.

____

## Changesets ##

| Version | Latest Change |
|---------|---------------|
| 0.0.0.1 | Add samples   |

### Setup ###

* Download Node.js from [Nodejs.org][1]
* Start the samples with node.js

### Who do I talk to? ###

* Contact me at [binaryfr3ak@gmail.com][2]
* Other community or team contact

[1]: http://nodejs.org
[2]: mailto:binaryfr3ak@gmail.com
