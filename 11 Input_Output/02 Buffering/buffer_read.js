buffer = new Buffer("Some UTF8 Text \u00b6 \u30c6 \u20ac", 'utf8');

console.log(buffer.toString());

console.log(buffer.toString('utf8', 5, 9));
var StringDecoder = require('string_decoder').StringDecoder;

var decoder = new StringDecoder('utf8');
console.log(decoder.write(buffer));

console.log(buffer[18].toString(16));

console.log(buffer.readUInt32BE(18).toString(16));


console.log("Length of buffer is %s because it contains also 2 etc. byte characters", buffer.length);
