var abc = new Buffer('abcdefghijklmnopqrstuvwxyz');

console.log(abc.toString());

//copy full buffer
var blank = new Buffer(26);
blank.fill();
console.log("blank: " + blank.toString());

abc.copy(blank);

console.log("blank: " + blank.toString());

//copy part of buffer
var dashes = new Buffer(26);
dashes.fill('-');
console.log("Dashes: " + dashes.toString());

abc.copy(dashes, 10, 10, 15);

console.log("Dashes: " + dashes.toString());

// copy to and from direct indexes of buffers
var dots = new Buffer('--------------------');
dots.fill('.');
console.log("dots :" + dots.toString());
for ( var i = 0; i < dots.length; i++)
{
	if(i %2)
	{
		dots[i] = abc[i];
	}
}

console.log("dots: " + dots.toString());
