var stream = require('stream'); 
var util = require('util'); 

util.inherits(Duplexer, stream.Duplex); 

function Duplexer(opt) { 
	stream.Duplex.call(this, opt); 
	this.data = []; 
} 

Duplexer.prototype._read = function readItem(size) { 
	var chunk = this.data.shift(); 
	if (chunk == "stop")
	{ 
		this.push(null); 
	}
	else
	{ 
		if(chunk)
		{ 
			this.push(chunk);
		}
		else 
		{
			setTimeout(readItem.bind(this), 500, size);
		}
	}
};

Duplexer.prototype._write = function(data, encoding, callback) { 
	this.data.push(data); 
	callback(); 
}; 

var duplex = new Duplexer(); 
duplex.on('data', function(chunk){ 
	console.log('read: ', chunk.toString()); 
}); 

duplex.on('end', function(){ 
	console.log('Message Complete'); 
}); 

duplex.write("I think, "); 
duplex.write("therefore "); 
duplex.write("I am."); 
duplex.write("Rene Descartes"); 
duplex.write("stop");