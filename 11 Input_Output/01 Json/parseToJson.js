var accountObj = {
	name: "Baggins",
	number: 1337,
	members: ["Frodo", "Bilbo"],
	location: "Shire"
};
var accountString = JSON.stringify(accountObj);
console.log(accountString);
