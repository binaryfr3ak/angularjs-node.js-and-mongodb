console.log("Here are the types of output which are available in node.js");

console.log("console.log([data], [...]);");

console.info("console.info([data], [...])");

console.error("console.error([data], [...])");

console.warn("console.warn([data], [...])");

console.log("console.dir(obj) will write a json (\'javascript object notation\') to the console");
console.dir({"name":"Florian", "born":"switzerland"});

console.log("The console.time(label) will measure the time used for a operation using the console.timeEnd(label) function");

console.time("SayHello");
console.log("Hello");
console.timeEnd("SayHello");

console.log("console.trace will write the trace to the stderr.");

console.trace("MyTrace");

console.log("console.assert is used to assert something.");

console.assert(true === true, "this is true");

console.assert(true===false,"this is not true");
