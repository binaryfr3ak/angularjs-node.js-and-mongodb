ExceptionWrapper();
ExceptionWrapper(-1);
ExceptionWrapper("");
ExceptionWrapper(3);


function ExceptionWrapper(number)
{
	try{
		IsPositiveNumber(number);
	}
	catch(error)
	{
		console.error("The error \'" + error.name + "\' with message \'" + error.message + "\' was catched");
	}
	finally
	{
		console.log("Even if there is an error, this code will be executed!");
	}
}



function IsPositiveNumber(number)
{
	if(number==null)
	{
		throw { name:"ArgumentNullException", message:"Argument \'" + number + "\' is null!"};
	}
	
	if(isNaN(number))
	{
		throw { name:"NotANumberException", message:"Argument \'" + number + "\' is not a number!"};
	}
	
	if(0>number)
	{
		throw { name: "NagativeNumberException", message: "Argument \'" + number + "\' is a negative number!"};
	}
	
	if(number=="")
	{
		throw { name: "NotANumberException", message: "Argument \'" + number + "\' is no a number!"};
	}

	
	console.log(number + " is a positive number");
}