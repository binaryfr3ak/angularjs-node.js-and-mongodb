var x=0;
var y=0;
var z=0;

function displayValues()
{
	console.log("X=%d, Y=%d, Z=%d", x, y, z);
}

function updateX()
{
	x++;
}

function updateY()
{
	y++;
}

function updateZ()
{
	z++;
	displayValues();
}

var myInterval = setInterval(updateX, 500);
setInterval(updateY, 1000);
setInterval(updateZ, 2000);

setTimeout(StopInterval, 20000, myInterval);

function StopInterval(id)
{
	clearInterval(id);
}



